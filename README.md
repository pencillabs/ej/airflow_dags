# Visão Geral

Repositório contendo a implementação do pipeline de dados da EJ. O pipeline é construido utilizando o projeto Airflow como framework, além de uma instância do Mongodb, responsável por armazenar o cruzamento dos dados, resultantes da execução do pipeline. Para mais informações, acesse a [wiki do projeto.](https://gitlab.com/pencillabs/ej/airflow_dags/-/wikis/Inicio)

# Quick start

1. Crie os diretórios de logs e plugins

	mkdir ./src/logs ./src/plugins

2. Prepare o banco de dados

	docker-compose up airflow-init

3. Execute a aplicação

	docker-compose up

4. Crie as conexões. Este passo é necessário para que o airflow saiba a URL da instância da EJ. Se você quiser apontar para uma instância local, execute o script e altere a conexão pela UI do airflow.

	docker exec -it airflow_server bash
	python dags/create_connections.py

Depois do ambiente configurado, você poderá acessar o airflow em `http://localhost:8080` . Para se autenticar, use o usuário `airflow`  e a senha `airflow` . O DAG de coleta se chama `ej_analysis_dag` .

# API

Você pode testar disparar o dag da EJ utilizando o arquivo do Postman (airflow.postman_collection.json) como exemplo.
